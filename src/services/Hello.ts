export class Hello {
    great(name: string) {
        return `Olá, ${name}`;
    }
}